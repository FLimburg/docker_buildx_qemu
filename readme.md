# docker_buildx_qemu

This Debian-based image allows you to easily build cross-platform images.
It's been tested with GitLab CI on gitlab.com, but it should work anywhere that docker-in-docker already works, and with a binfmt_misc enabled kernel.

Heavily inspired by https://gitlab.com/com.hillnz.docker/docker-buildx-qemu


## Example Usage

This GitLab example should give you an idea of how to use the image.
https://gitlab.com/snorrelo/docker_buildx_usage_example

